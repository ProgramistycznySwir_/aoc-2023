use super::Vec2;

pub struct Map2D<T>(pub Vec<Vec<T>>);

impl<T> Map2D<T> {
    pub fn get_by_row_col(&self, row: usize, col: usize) -> Option<&T> {
        self.0.get(row)?.get(col)
    }

    pub fn get(&self, vec: Vec2<i32>) -> Option<&T> {
        if !self.inbounds(vec) {
            None
        } else {
            self.get_by_row_col(vec.y as usize, vec.x as usize)
        }
    }

    pub fn set(&mut self, vec: Vec2<i32>, val: T) -> Option<()> {
        if !self.inbounds(vec) {
            None
        } else {
            self.0[vec.y as usize][vec.x as usize] = val;
            Some(())
        }
    }

    pub fn find(&self, pred: impl Fn(&T) -> bool) -> Option<&T> {
        self.get(self.find_pos(pred)?.into())
    }

    /// Find positions of all items that fulfill predicate
    pub fn find_all(&self, pred: impl Fn(&T) -> bool) -> Vec<Vec2<usize>> {
        let mut result = Vec::new();
        for row in 0..self.rows() {
            for col in 0..self.cols() {
                if pred(self.get_by_row_col(row, col).unwrap()) {
                    result.push(Vec2::new(col, row));
                }
            }
        }
        result
    }

    /// returns `(row, col)`
    pub fn find_pos(&self, pred: impl Fn(&T) -> bool) -> Option<Vec2<usize>> {
        for row in 0..self.rows() {
            for col in 0..self.cols() {
                if pred(self.get_by_row_col(row, col).unwrap()) {
                    return Some(Vec2::new(col, row));
                }
            }
        }
        None
    }

    pub fn row(&self, row: usize) -> Option<&Vec<T>> {
        self.0.get(row)
    }

    pub fn inbounds(&self, vec: Vec2<i32>) -> bool {
        if vec.x < 0 || vec.y < 0 {
            false
        } else if vec.x >= self.cols() as i32 || vec.y >= self.rows() as i32 {
            false
        } else {
            true
        }
    }

    pub fn rows(&self) -> usize {
        self.0.len()
    }
    pub fn cols(&self) -> usize {
        self.0.get(0).unwrap().len()
    }
}