use std::ops::{Neg, Sub, Add};


#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Vec2<T> {
    pub x: T,
    pub y: T,
}

impl<T> Vec2<T> {
    pub fn new(x: T, y: T) -> Self {
        Self { x, y }
    }
}

impl Vec2<i32> {
    pub(crate) const RIGHT: Vec2<i32> = Self { x: 1, y: 0 };
    pub(crate) const LEFT: Vec2<i32> = Self { x: -1, y: 0 };
    pub(crate) const UP: Vec2<i32> = Self { x: 0, y: 1 };
    pub(crate) const DOWN: Vec2<i32> = Self { x: 0, y: -1 };
    pub(crate) const ONE: Vec2<i32> = Self { x: 1, y: 1 };
}

impl Into<Vec2<i32>> for Vec2<usize> {
    fn into(self) -> Vec2<i32> {
        Vec2::new(self.x as i32, self.y as i32)
    }
}
impl<T> From<Vec2<T>> for (T, T) {
    fn from(vec: Vec2<T>) -> (T, T) {
        (vec.x, vec.y)
    }
}

impl<T> Add for Vec2<T>
where
    T: std::ops::Add<Output = T>,
{
    type Output = Vec2<T>;

    fn add(self, rhs: Self) -> Self::Output {
        Vec2::new(self.x + rhs.x, self.y + rhs.y)
    }
}

impl<T> Sub for Vec2<T>
where
    T: std::ops::Sub<Output = T>,
{
    type Output = Vec2<T>;

    fn sub(self, rhs: Self) -> Self::Output {
        Vec2::new(self.x - rhs.x, self.y - rhs.y)
    }
}

impl<T: Neg<Output = T>> Neg for Vec2<T> {
    type Output = Self;

    fn neg(self) -> Self::Output {
        Vec2::new(-self.x, -self.y)
    }
}
