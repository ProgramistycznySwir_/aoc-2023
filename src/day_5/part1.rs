use std::{borrow::BorrowMut, cmp::min, collections::HashMap};

use super::{Map, Mapping};
use crate::read_lines;

#[allow(dead_code)]
pub fn solution_1(filename: &str) -> u64 {
    let mut file = read_lines(format!("./src/day_5/{}", filename))
        .unwrap()
        .flatten();

    let first_line = file.next().unwrap();
    let seeds = first_line.split(' ').skip(1).map(str::parse::<u64>);
    let mut maps: HashMap<String, Map> = HashMap::new();
    let mut curr_map: Option<Map> = None;

    for line in file.skip(1) {
        let first_char = line.chars().next();
        if let Some(first_char) = first_char {
            if first_char.is_digit(10) {
                if let Some(map) = curr_map.borrow_mut() {
                    map.mappings.push(Mapping::from_line(&line))
                }
            } else {
                curr_map = Some(Map::from_line(line));
            }
        } else if let Some(map) = curr_map.borrow_mut() {
            maps.insert(map.from.to_owned(), map.sort().to_owned());
            curr_map = None;
        }
    }
    if let Some(map) = curr_map.borrow_mut() {
        maps.insert(map.from.to_owned(), map.sort().to_owned());
    }

    let mut lowest = u64::MAX;
    for seed in seeds.flatten() {
        let mut map = maps.get("seed").unwrap();
        let mut curr_val = map.map(seed);

        while map.to != "location" {
            map = maps.get(&map.to).unwrap();
            curr_val = map.map(curr_val);
        }

        lowest = min(lowest, curr_val);
    }

    lowest
}

#[cfg(test)]
mod tests {
    #[test]
    fn solution_1() {
        assert_eq!(super::solution_1("input"), 218513636);
    }

    #[test]
    fn solution_1_test_cases() {
        assert_eq!(super::solution_1("test"), 35);
    }
}
