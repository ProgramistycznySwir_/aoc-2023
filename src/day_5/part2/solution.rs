use std::{
    borrow::BorrowMut,
    cmp::min,
    collections::HashMap,
};

use super::super::{Map, Mapping};
use super::range::Range;
use crate::read_lines;

#[allow(dead_code)]
pub fn solution(filename: &str) -> u64 {
    let mut file = read_lines(format!("./src/day_5/{}", filename))
        .unwrap()
        .flatten();

    let first_line = file.next().unwrap();
    let seeds_raw = first_line
        .split(' ')
        .skip(1)
        .map(str::parse::<u64>)
        .flatten()
        .collect::<Vec<u64>>();
    let seed_ranges = seeds_raw.chunks(2).map(|pair| Range::new(pair[0], pair[1]));

    let mut maps: HashMap<String, Map> = HashMap::new();
    let mut curr_map: Option<Map> = None;

    for line in file.skip(1) {
        let first_char = line.chars().next();
        if let Some(first_char) = first_char {
            if first_char.is_digit(10) {
                if let Some(map) = curr_map.borrow_mut() {
                    map.mappings.push(Mapping::from_line(&line))
                }
            } else {
                curr_map = Some(Map::from_line(line));
            }
        } else if let Some(map) = curr_map.borrow_mut() {
            maps.insert(map.from.to_owned(), map.sort().to_owned());
            curr_map = None;
        }
    }
    if let Some(map) = curr_map.borrow_mut() {
        maps.insert(map.from.to_owned(), map.sort().to_owned());
    }

    let mut lowest = u64::MAX;
    for seed_range in seed_ranges {
        let mut map = maps.get("seed").unwrap();
        let mut curr_ranges = map.map_range(&seed_range);

        while map.to != "location" {
            map = maps.get(&map.to).unwrap();
            curr_ranges = curr_ranges
                .iter()
                .map(|range| map.map_range(range))
                .flatten()
                .collect();
        }

        curr_ranges = Range::sort_and_merge_mappings(curr_ranges);

        lowest = min(lowest, curr_ranges.iter().min().unwrap().from);
    }

    lowest
}

#[cfg(test)]
mod tests {
    #[test]
    fn solution() {
        assert_eq!(super::solution("input"), 81956384);
    }
    #[test]
    fn solution_test_cases() {
        assert_eq!(super::solution("test"), 46);
    }
}
