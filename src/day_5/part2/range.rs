use std::cmp::{max, min};


#[derive(Clone, Debug)]
pub struct Range {
    pub from: u64,
    pub lenght: u64,
}

impl PartialEq for Range {
    fn eq(&self, other: &Self) -> bool {
        self.from == other.from && self.lenght == other.lenght
    }
}
impl PartialOrd for Range {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        self.from.partial_cmp(&other.from)
    }
}
impl Eq for Range {}
impl Ord for Range {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.partial_cmp(other).unwrap()
    }
}

impl From<[u64; 2]> for Range {
    fn from(value: [u64; 2]) -> Self {
        Range::new(value[0], value[1] - value[0] + 1)
    }
}

impl Range {
    pub fn new(from: u64, lenght: u64) -> Self {
        Self { from, lenght }
    }

    pub fn overlap(&self, other: &Self) -> Option<Self> {
        let lower = min(self, other);
        let upper = max(self, other);
        let start_diff = upper.from - lower.from;
        if start_diff >= lower.lenght {
            return None;
        }

        let lenght = min(lower.lenght - start_diff, upper.lenght);
        Some(Range::new(upper.from, lenght))
    }

    pub fn overlaps(&self, other: &Self) -> bool {
        let lower = min(self, other);
        let upper = max(self, other);
        let start_diff = upper.from - lower.from;
        return start_diff < lower.lenght;
    }

    pub fn shift(mut self, by: &i64) -> Range {
        self.from = (self.from as i64 + by) as u64;
        self
    }

    pub fn end(&self) -> u64 {
        self.from
            .checked_add(self.lenght)
            .expect(format!("{:?}", self).as_str())
    }

    pub fn sort_and_merge_mappings(mut arr: Vec<Self>) -> Vec<Range> {
        if arr.is_empty() {
            return arr;
        }

        arr.sort();
        let mut result = vec![arr.first().unwrap().clone()];

        for range in arr.iter().skip(1) {
            let last = result.last_mut().unwrap();
            if last.overlaps(range) {
                last.extend(range);
            } else {
                result.push(range.clone());
            }
        }
        result
    }

    pub fn extend(&mut self, by: &Self) {
        self.lenght = by.end() - self.from
    }
}

#[cfg(test)]
mod tests {
    use std::cmp::{max, min};

    use crate::day_5::Mapping;

    use super::Range;

    #[test]
    fn equalities() {
        let a = Range::new(1, 2);
        let b = Range::new(3, 4);
        assert_eq!(min(&a, &b), &a);
        assert_eq!(max(&a, &b), &b);

        let c = Range::new(1, 2);
        let d = Range::new(1, 4);
        assert_eq!(&a, &c);
        assert_ne!(&a, &d);
    }

    #[test]
    fn from_u64_slice() {
        assert_eq!(Range::new(1, 2), [1, 2].into());
        assert_eq!(Range::new(1, 10), [1, 10].into());
        assert_eq!(Range::new(10, 10), [10, 19].into());
    }

    #[test]
    fn overlap_test() {
        let a = Range::new(1, 4);
        let b = Range::new(3, 4);
        assert_eq!(Range::overlap(&a, &b), Some(Range::new(3, 2)));
        let c = Range::new(2, 2);
        assert_eq!(Range::overlap(&a, &c), Some(Range::new(2, 2)));
        assert_eq!(Range::overlap(&b, &c), Some(Range::new(3, 1)));
        let d = Range::new(4, 4);
        assert_eq!(Range::overlap(&a, &d), Some(Range::new(4, 1)));
        assert_eq!(Range::overlap(&c, &d), None);

        let range = Range::from([1, 10]);
        let map = Mapping::from_line("15 10 5").source();
        assert_eq!(range.overlap(&map), Some(Range::new(10, 1)));
    }
}