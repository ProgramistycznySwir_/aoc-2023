pub mod range;
pub mod solution;
pub mod solution_parallel;

use super::{Map, Mapping};
use range::Range;

impl Map {
    fn map_range(&self, range: &Range) -> Vec<Range> {
        let mut result: Vec<Range> = Vec::new();
        let mut curr = range.from;
        for map in self.mappings.iter() {
            let source = map.source();
            if range.end() >= source.from && curr < source.end() {
                if curr < source.from {
                    result.push(Range::from([curr, source.from - 1]));
                }
                if let Some(overlap) = source.overlap(range) {
                    result.push(overlap.shift(&map.get_shift()));
                }
            }
            curr = source.end();
            if curr >= range.end() {
                break;
            }
        }
        if result.is_empty() {
            result.push(range.clone());
        }
        result
    }
}

impl Mapping {
    fn source(&self) -> Range {
        Range::new(self.source_start, self.range_lenght)
    }

    fn get_shift(&self) -> i64 {
        self.destination_start as i64 - self.source_start as i64
    }
}


#[cfg(test)]
mod tests {
    mod mapping {
        use crate::day_5::Mapping;

        use super::super::Range;

        #[test]
        fn source_test() {
            let mapping = Mapping::from_line("20 10 10");
            assert_eq!(mapping.source(), Range::from([10, 19]));
            assert_eq!(mapping.get_shift(), 10);

            let mapping = Mapping::from_line("20 10 5");
            assert_eq!(mapping.source(), Range::from([10, 14]));
            assert_eq!(mapping.get_shift(), 10);
        }
    }
    mod map {
        use crate::day_5::{Map, Mapping};

        use super::super::Range;

        #[test]
        fn map_range_test() {
            let map = Map {
                from: "".to_owned(),
                to: "".to_owned(),
                mappings: vec![
                    Mapping::from_line("15 10 5"),
                    Mapping::from_line("200 100 5"),
                ],
            };
            let mut res = map.map_range(&Range::from([1, 1]));
            assert_eq!(res, vec![Range::from([1, 1])]);
            res = map.map_range(&Range::from([1, 10]));
            assert_eq!(res, vec![Range::from([1, 9]), Range::from([15, 15])]);

            let map = Map {
                from: "".to_owned(),
                to: "".to_owned(),
                mappings: vec![
                    Mapping::from_line("1500 10 5"),
                    Mapping::from_line("200 100 5"),
                ],
            };
            res = map.map_range(&Range::from([1, 150]));
            res = super::super::Range::sort_and_merge_mappings(res);
            assert_eq!(
                res,
                vec![
                    Range::from([1, 9]),
                    Range::from([15, 99]),
                    Range::from([200, 204]),
                    Range::from([1500, 1504]),
                ]
            );
        }
    }
}