mod part1;
pub use part1::solution_1;
mod part2;
pub use part2::solution::solution;
pub use part2::solution_parallel::solution_parallel;

#[derive(Clone, Debug)]
struct Map {
    from: String,
    to: String,
    mappings: Vec<Mapping>,
}

impl Map {
    fn from_line(line: String) -> Map {
        let mut values = line.split(' ').next().unwrap().split('-');
        Map {
            from: values.next().unwrap().to_owned(),
            to: values.skip(1).next().unwrap().to_owned(),
            mappings: Vec::new(),
        }
    }

    fn sort(&mut self) -> &mut Self {
        self.mappings.sort_by_key(|mapping| mapping.source_start);
        self
    }

    fn map(&self, val: u64) -> u64 {
        for map in self.mappings.iter() {
            let diff = val as i64 - map.source_start as i64;
            if diff >= 0 && diff < map.range_lenght as i64 {
                return map.destination_start + diff as u64;
            }
        }
        val
    }
}

#[derive(Clone, Debug)]
struct Mapping {
    destination_start: u64,
    source_start: u64,
    range_lenght: u64,
}

impl Mapping {
    fn from_line(line: &str) -> Mapping {
        let mut values = line.split(' ');
        Mapping {
            destination_start: values.next().unwrap().parse::<u64>().unwrap(),
            source_start: values.next().unwrap().parse::<u64>().unwrap(),
            range_lenght: values.next().unwrap().parse::<u64>().unwrap(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::{Map, Mapping};

    #[test]
    fn Map_map() {
        let map = Map {
            from: "".to_owned(),
            to: "".to_owned(),
            mappings: vec![
                Mapping::from_line("150 50 5"),
                Mapping::from_line("200 100 5"),
            ],
        };
        assert_eq!(map.map(1), 1);
        assert_eq!(map.map(49), 49);
        assert_eq!(map.map(50), 150);
        assert_eq!(map.map(54), 154);
        assert_eq!(map.map(55), 55);
        assert_eq!(map.map(100), 200);
        assert_eq!(map.map(104), 204);
        assert_eq!(map.map(105), 105);
    }
}
