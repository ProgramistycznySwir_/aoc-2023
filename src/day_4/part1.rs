use itertools::Itertools;

use crate::read_lines;

pub fn solution(filename: &str) -> usize {
    read_lines(format!("./src/day_4/{}", filename))
        .unwrap()
        .flatten()
        .map(|line| {
            let (left, right) = line.split('|').collect_tuple().unwrap();
            let winning: Vec<&str> = left.split(' ').skip(2).filter(|s| !s.is_empty()).collect();
            let count = right
                .split(' ')
                .filter(|number| winning.contains(number))
                .count();
            if count == 0 {
                0
            } else {
                usize::pow(2, count as u32 - 1)
            }
        })
        .sum()
}

#[cfg(test)]
mod tests {
    #[test]
    fn solution() {
        assert_eq!(super::solution("input"), 23028);
    }

    #[test]
    fn solution_test_cases() {
        assert_eq!(super::solution("test0"), 13);
    }
}
