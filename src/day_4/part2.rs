use itertools::Itertools;

use crate::read_lines;

pub fn solution(filename: &str) -> i32 {
    let cards: Vec<usize> = read_lines(format!("./src/day_4/{}", filename))
        .unwrap()
        .flatten()
        .map(|line| {
            let (left, right) = line.split('|').collect_tuple().unwrap();
            let winning: Vec<&str> = left.split(' ').skip(2).filter(|s| !s.is_empty()).collect();
            let count = right
                .split(' ')
                .filter(|number| winning.contains(number))
                .count();
            count
        })
        .collect();

    let mut copies = vec![1; cards.len()];

    for (card_idx, card) in cards.iter().enumerate() {
        ((card_idx + 1)..(card_idx + *card + 1)).for_each(|i| copies[i] += copies[card_idx]);
    }

    copies.iter().sum()
}

#[cfg(test)]
mod tests {
    #[test]
    fn solution() {
        assert_eq!(super::solution("input"), 9236992);
    }

    #[test]
    fn solution_test_cases() {
        assert_eq!(super::solution("test0"), 30);
    }
}
