use std::iter::zip;

use crate::read_lines;

#[allow(dead_code)]
pub fn solution(filename: &str) -> u32 {
    let mut file = read_lines(format!("./src/day_6/{}", filename))
        .unwrap()
        .flatten();

    zip(
        file.next()
            .unwrap()
            .split(' ')
            .skip(1)
            .map(str::parse::<u32>)
            .flatten(),
        file.next()
            .unwrap()
            .split(' ')
            .skip(1)
            .map(str::parse::<u32>)
            .flatten(),
    )
    .map(|(time, dist)| {
        (0..time)
            .filter(|acceleration_time| dist < acceleration_time * (time - acceleration_time))
            .count() as u32
    })
    .fold(1, u32::wrapping_mul)
}

#[cfg(test)]
mod tests {
    #[test]
    fn solution() {
        assert_eq!(super::solution("input"), 512295);
    }

    #[test]
    fn solution_test_cases() {
        assert_eq!(super::solution("test"), 288);
    }
}
