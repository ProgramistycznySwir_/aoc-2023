use crate::read_lines;

#[allow(dead_code)]
pub fn solution(filename: &str) -> u64 {
    let mut file = read_lines(format!("./src/day_6/{}", filename))
        .unwrap()
        .flatten();

    let time = file
        .next()
        .unwrap()
        .split(' ')
        .skip(1)
        .collect::<String>()
        .parse::<u64>()
        .unwrap();
    let dist = file
        .next()
        .unwrap()
        .split(' ')
        .skip(1)
        .collect::<String>()
        .parse::<u64>()
        .unwrap();

    (0..time)
        .filter(|acceleration_time| dist < acceleration_time * (time - acceleration_time))
        .count() as u64
}

#[cfg(test)]
mod tests {
    #[test]
    fn solution() {
        assert_eq!(super::solution("input"), 36530883);
    }

    #[test]
    fn solution_test_cases() {
        assert_eq!(super::solution("test"), 71503);
    }
}
