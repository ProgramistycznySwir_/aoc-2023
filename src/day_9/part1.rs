use crate::read_lines;

#[allow(dead_code)]
pub fn solution(filename: &str) -> i64 {
    read_lines(format!("./src/day_9/{}", filename))
        .unwrap()
        .flatten()
        .map(|raw_values| {
            raw_values
                .split(' ')
                .map(|raw_value| raw_value.parse().to_owned())
                .flatten()
                .collect()
        })
        .map(|values: Vec<i64>| {
            let mut rows: Vec<Vec<i64>> = vec![values];
            while !rows.last().unwrap().iter().all(|val| val == &0) {
                rows.push(
                    rows.last()
                        .unwrap()
                        .windows(2)
                        .map(|window| window[1] - window[0])
                        .collect(),
                );
            }

            rows.iter()
                .map(|row| row.last().unwrap())
                .rev()
                .fold(0, |aggr, curr| curr + aggr)
        })
        .sum()
}

#[cfg(test)]
mod tests {
    #[test]
    fn solution() {
        assert_eq!(super::solution("input"), 1904165718);
    }

    #[test]
    fn solution_test_cases() {
        assert_eq!(super::solution("test_single_line"), 18);
        assert_eq!(super::solution("test0"), 114);
    }
}
