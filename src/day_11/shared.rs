
use rayon::prelude::*;

use crate::{read_lines, shared::{Map2D, Vec2}};

pub fn solution(filename: &str, expansion_factor: usize) -> i64 {
    let file = read_lines(filename)
        .unwrap()
        .flatten();

    let map = Map2D(file.map(|line| line.bytes().collect()).collect());

    let mut galaxies = map.find_all(|val| val == &b'#');

    let empty_cols: Vec<usize> = (0..map.cols())
        .filter(|col| galaxies.iter().all(|galaxy| &galaxy.x != col))
        .collect();
    let empty_rows: Vec<usize> = (0..map.rows())
        .filter(|row| galaxies.iter().all(|galaxy| &galaxy.y != row))
        .collect();

    galaxies.par_iter_mut().for_each(|galaxy| {
        galaxy.x += (expansion_factor - 1) * empty_cols.iter().filter(|col| col < &&galaxy.x).count();
        galaxy.y += (expansion_factor - 1) * empty_rows.iter().filter(|row| row < &&galaxy.y).count();
    });

    galaxies
        .par_iter()
        .enumerate()
        .map(|(i, galaxy)| {
            galaxies
                .iter()
                .skip(i + 1)
                .map(|other_galaxy| tile_distance((*galaxy).into(), (*other_galaxy).into()) as i64)
                .sum::<i64>()
                // .fold(0, |aggr, curr| { aggr + curr })
        })
        .sum()
}

fn tile_distance(a: Vec2<i32>, b: Vec2<i32>) -> i32 {
    let (x, y) = (a - b).into();
    x.abs() + y.abs()
}


#[cfg(test)]
mod tests {
    #[test]
    fn solution_test_cases() {
        assert_eq!(super::solution("./src/day_11/test0", 2), 374);
        assert_eq!(super::solution("./src/day_11/test0", 10), 1030);
        assert_eq!(super::solution("./src/day_11/test0", 100), 8410);
    }
}