pub fn solution(filename: &str) -> i64 {
    super::shared::solution(format!("./src/day_11/{}", filename).as_str(), 1000000)
}

#[cfg(test)]
mod tests {
    #[test]
    fn solution() {
        assert_eq!(super::solution("input"), 504715068438);
    }
}
