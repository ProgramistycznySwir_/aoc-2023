pub fn solution(filename: &str) -> i64 {
    super::shared::solution(format!("./src/day_11/{}", filename).as_str(), 2)
}

#[cfg(test)]
mod tests {
    #[test]
    fn solution() {
        assert_eq!(super::solution("input"), 10077850);
    }
}
