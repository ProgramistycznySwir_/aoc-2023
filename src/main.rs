use std::{
    fs::File,
    io::{self, BufRead},
    path::Path
};

mod shared;
mod day_1;
mod day_2;
mod day_3;
mod day_4;
mod day_5;
mod day_6;
mod day_8;
mod day_9;
mod day_10;
mod day_11;

fn main() {
    println!("Day 1 solution: {}", day_1::solution_1());
    println!("Day 1 part 2 solution: {}", day_1::solution_2());
    println!("Day 2 solution: {}", day_2::solution_1());
    println!("Day 2 part 2 solution: {}", day_2::solution_2());
    println!("Day 3 solution: {}", day_3::solution_1("input"));
    println!("Day 4 solution: {}", day_4::part1("input"));
    println!("Day 4 part 2 solution: {}", day_4::part2("input"));
    println!("Day 5 solution: {}", day_5::solution_1("input"));
    println!("Day 5 part 2 solution: {}", day_5::solution("input"));
    // println!("Day 5 part 2 (parallelized) solution: {}", day_5::solution_parallel("input"));
    println!("Day 6 solution: {}", day_6::part1_solution("input"));
    println!("Day 6 part 2 solution: {}", day_6::part2_solution("input"));
    println!("Day 8 solution: {}", day_8::part1_solution("input"));
    println!("Day 8 part 2 solution: {}", day_8::part2_solution("input"));
    println!("Day 9 solution: {}", day_9::part1("input"));
    println!("Day 9 part 2 solution: {}", day_9::part2("input"));
    println!("Day 10 solution: {}", day_10::part1("input"));
    println!("Day 10 part 2 solution: {}", day_10::part2("input"));
    println!("Day 11 solution: {}", day_11::part1("input"));
    println!("Day 11 part 2 solution: {}", day_11::part2("input"));
}

/// Stolen from docs (https://doc.rust-lang.org/rust-by-example/std_misc/file/read_lines.html#a-more-efficient-approach)
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}
