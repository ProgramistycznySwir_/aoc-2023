use crate::read_lines;

#[allow(dead_code)]
pub fn solution_1(filename: &str) -> i32 {
    let file: Vec<Vec<u8>> = read_lines(format!("./src/day_3/{}", filename))
        .unwrap()
        .flatten()
        .map(|line| line.as_bytes().to_owned())
        .collect();
    let get = |row: usize, col: usize| file.get(row)?.get(col);

    let mut sum = 0;
    for row in 0..file.len() {
        let mut start: Option<usize> = None;
        for col in 0..file.get(row).unwrap().len() + 1 {
            let c = get(row, col);
            if c.is_some() && c.unwrap().is_ascii_digit() {
                if start.is_none() {
                    start = Some(col);
                }
            } else if let Some(word_start) = start {
                let end = col;

                let is_part = skip_take(file.iter(), row as i32 - 1, 3).any(|row| -> bool {
                    skip_take(row.iter(), word_start as i32 - 1, end - word_start + 2)
                        .any(|c| !c.is_ascii_digit() && c != &b'.')
                });

                if is_part {
                    let part_no =
                        String::from_utf8(file.get(row).unwrap()[word_start..end].to_vec())
                            .unwrap()
                            .parse::<i32>()
                            .unwrap();

                    sum += part_no;
                }

                start = None;
            }
        }
    }

    sum
}

fn skip_take<T>(
    iter: impl Iterator<Item = T>,
    skip: i32,
    take: usize,
) -> std::iter::Take<std::iter::Skip<impl Iterator<Item = T>>> {
    let take = (take as i32 + skip.min(0)).max(0) as usize;
    let skip = skip.max(0) as usize;
    iter.skip(skip).take(take)
}

#[cfg(test)]
mod tests {
    #[test]
    fn solution_1() {
        assert_eq!(super::solution_1("input"), 540131);
    }

    #[test]
    fn solution_1_test_cases() {
        assert_eq!(super::solution_1("test"), 4361);
        assert_eq!(super::solution_1("test2"), 7253);
    }

    #[test]
    fn skip_take() {
        assert!(super::skip_take([1, 2, 3].iter(), 0, 3).eq([1, 2, 3].iter()));
        assert!(super::skip_take([1, 2, 3].iter(), -1, 3).eq([1, 2].iter()));
        assert!(super::skip_take([1, 2, 3].iter(), 1, 3).eq([2, 3].iter()));
        assert!(super::skip_take([1, 2, 3].iter(), 2, 3).eq([3].iter()));
        assert!(super::skip_take([1, 2, 3].iter(), 3, 3).eq([].iter()));
    }
}
