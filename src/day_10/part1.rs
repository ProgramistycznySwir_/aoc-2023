use crate::{read_lines, shared::{Vec2, Map2D}};

pub fn solution(filename: &str) -> i64 {
    let file = read_lines(format!("./src/day_10/{}", filename))
        .unwrap()
        .flatten();

    let map = Map2D(file.map(|line| line.bytes().collect()).collect());

    let starting_pos = map.find_pos(|c| c == &b'S').unwrap().into();
    let mut crawlers: Vec<Crawler> = vec![Vec2::UP, Vec2::DOWN, Vec2::RIGHT, Vec2::LEFT]
        .iter()
        .map(|dirr| Crawler {
            prev: starting_pos,
            curr: starting_pos + *dirr,
        })
        .collect();

    let mut dist = 1;
    loop {
        if crawlers
            .iter()
            .any(|crawler| map.get(crawler.curr) == Some(&b'S'))
        {
            break;
        }
        crawlers = crawlers
            .iter_mut()
            .map(|crawler| crawler.crawl(map.get(crawler.curr)?))
            .flatten()
            .collect();
        dist += 1;
    }

    dist / 2
}

#[derive(Debug, Clone, Copy)]
struct Crawler {
    prev: Vec2<i32>,
    curr: Vec2<i32>,
}

impl Crawler {
    fn crawl(mut self, pipe_section: &u8) -> Option<Self> {
        let diff = self.curr - self.prev;
        let turn = match (pipe_section, diff) {
            (b'|' | b'-', _) => Some(diff),
            (b'F', Vec2::DOWN) => Some(Vec2::RIGHT),
            (b'F', Vec2::LEFT) => Some(Vec2::UP),
            (b'L', Vec2::UP) => Some(Vec2::RIGHT),
            (b'L', Vec2::LEFT) => Some(Vec2::DOWN),
            (b'J', Vec2::UP) => Some(Vec2::LEFT),
            (b'J', Vec2::RIGHT) => Some(Vec2::DOWN),
            (b'7', Vec2::DOWN) => Some(Vec2::LEFT),
            (b'7', Vec2::RIGHT) => Some(Vec2::UP),
            _ => None,
        };

        if let Some(turn) = turn {
            let temp = self.curr;
            self.curr = self.curr + turn;
            self.prev = temp;
            Some(self)
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{day_10::part1::Crawler, shared::Vec2};

    #[test]
    fn solution() {
        assert_eq!(super::solution("input"), 6701);
    }

    #[test]
    fn solution_test_cases() {
        assert_eq!(super::solution("test0"), 4);
        assert_eq!(super::solution("test1"), 8);
    }

    #[test]
    fn crawler_crawl_test() {
        let sut = Crawler {
            prev: Vec2::new(1, 1),
            curr: Vec2::new(2, 1),
        };
        assert_eq!(sut.crawl(&b'-').unwrap().curr, Vec2::new(3, 1));
        let sut = Crawler {
            prev: Vec2::new(2, 1),
            curr: Vec2::new(1, 1),
        };
        assert_eq!(sut.crawl(&b'F').unwrap().curr, Vec2::new(1, 2));
        let sut = Crawler {
            prev: Vec2::new(1, 2),
            curr: Vec2::new(1, 1),
        };
        assert_eq!(sut.crawl(&b'F').unwrap().curr, Vec2::new(2, 1));
        let sut = Crawler {
            prev: Vec2::new(2, 1),
            curr: Vec2::new(1, 1),
        };
        assert_eq!(sut.crawl(&b'L').unwrap().curr, Vec2::new(1, 0));
        let sut = Crawler {
            prev: Vec2::new(1, 1),
            curr: Vec2::new(1, 2),
        };
        assert_eq!(sut.crawl(&b'L').unwrap().curr, Vec2::new(2, 2));
    }
}
