use core::panic;
use std::collections::HashSet;

use rayon::iter::{IntoParallelIterator, ParallelIterator};

use crate::{read_lines, shared::{Vec2, Map2D}};

#[allow(dead_code)]
pub fn solution(filename: &str) -> i64 {
    let file = read_lines(format!("./src/day_10/{}", filename))
        .unwrap()
        .flatten();

    let mut map = Map2D(file.map(|line| line.bytes().collect()).collect());

    let starting_pos = map.find_pos(|c| c == &b'S').unwrap().into();
    let mut crawlers: Vec<Crawler> = vec![Vec2::UP, Vec2::DOWN, Vec2::RIGHT, Vec2::LEFT]
        .iter()
        .map(|dirr| Crawler {
            prev: starting_pos,
            curr: starting_pos + *dirr,
            path: vec![starting_pos, starting_pos + *dirr],
        })
        .collect();

    loop {
        if crawlers
            .iter()
            .any(|crawler| map.get(crawler.curr) == Some(&b'S'))
        {
            break;
        }
        for crawler_idx in 0..crawlers.len() {
            if let Some(crawler) = crawlers.get_mut(crawler_idx) {
                if let Some(pipe_segment) = map.get(crawler.curr) {
                    crawler.crawl(pipe_segment);
                }
            }
        }
    }

    let crawler = crawlers
        .iter()
        .max_by_key(|crawler| crawler.path.len())
        .unwrap();

    let path = &crawler.path;

    // Replace S with appropriate symbol
    let after_pos = path[1] - starting_pos;
    let before_pos = path[path.len() - 2] - starting_pos;
    map.set(
        starting_pos,
        match (after_pos, before_pos) {
            (Vec2::RIGHT, Vec2::LEFT) | (Vec2::LEFT, Vec2::RIGHT) => b'-',
            (Vec2::DOWN, Vec2::UP) | (Vec2::UP, Vec2::DOWN) => b'|',
            (Vec2::RIGHT, Vec2::UP) | (Vec2::UP, Vec2::RIGHT) => b'F',
            (Vec2::RIGHT, Vec2::DOWN) | (Vec2::DOWN, Vec2::RIGHT) => b'L',
            (Vec2::LEFT, Vec2::UP) | (Vec2::UP, Vec2::LEFT) => b'7',
            (Vec2::LEFT, Vec2::DOWN) | (Vec2::DOWN, Vec2::LEFT) => b'J',
            _ => panic!("Piping is all wrong"),
        },
    )
    .expect("Starting pos is outside map??");

    (0..map.rows())
        .into_par_iter()
        .map(|row| {
            let row_path: HashSet<i32> = HashSet::from_iter(
                path.iter()
                    .filter(|path_segment| path_segment.y == row as i32)
                    .map(|vec| vec.x),
            );

            let mut sum = 0;
            let mut inside = false;
            for (i, c) in map.row(row).unwrap().iter().enumerate() {
                // How do I arrived at this solution? Just extend the map by 2 in vertical as if:
                // F7|L-J -> F7|L-J
                //           |||
                // and count the | (I here avoid step of enlargeing map perpendicularly to traversing dirrection, just treat F and 7 as vertical pieces)
                if row_path.contains(&(i as i32)) {
                    if c == &b'|' || c == &b'F' || c == &b'7' {
                        inside = !inside;
                    }
                } else if inside {
                    sum += 1;
                }
            }

            sum
        })
        .sum()
}

#[derive(Debug, Clone)]
struct Crawler {
    prev: Vec2<i32>,
    curr: Vec2<i32>,
    path: Vec<Vec2<i32>>,
}

impl Crawler {
    fn crawl(&mut self, pipe_section: &u8) -> Option<&mut Self> {
        let diff = self.curr - self.prev;
        let turn = match (pipe_section, diff) {
            (b'|' | b'-', _) => Some(diff),
            (b'F', Vec2::DOWN) => Some(Vec2::RIGHT),
            (b'F', Vec2::LEFT) => Some(Vec2::UP),
            (b'L', Vec2::UP) => Some(Vec2::RIGHT),
            (b'L', Vec2::LEFT) => Some(Vec2::DOWN),
            (b'J', Vec2::UP) => Some(Vec2::LEFT),
            (b'J', Vec2::RIGHT) => Some(Vec2::DOWN),
            (b'7', Vec2::DOWN) => Some(Vec2::LEFT),
            (b'7', Vec2::RIGHT) => Some(Vec2::UP),
            _ => None,
        };

        if let Some(turn) = turn {
            let temp = self.curr;
            self.curr = self.curr + turn;
            self.prev = temp;
            self.path.push(self.curr);
            Some(self)
        } else {
            None
        }
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn solution() {
        assert_eq!(super::solution("input"), 303);
    }

    #[test]
    fn solution_test_cases() {
        assert_eq!(super::solution("test2"), 4);
        assert_eq!(super::solution("test3"), 10);
    }
}
