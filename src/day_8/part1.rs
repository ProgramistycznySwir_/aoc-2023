use std::collections::HashMap;

use text_io::scan;

use crate::read_lines;

#[allow(dead_code)]
pub fn solution(filename: &str) -> u32 {
    let mut file = read_lines(format!("./src/day_8/{}", filename))
        .unwrap()
        .flatten();

    let first_line = file.next().unwrap();

    let map: HashMap<String, (String, String)> = HashMap::from_iter(file.skip(1).map(|line| {
        let crossing_id: String;
        let left: String;
        let right: String;
        scan!(line.bytes() => "{} = ({}, {})", crossing_id, left, right);
        (crossing_id, (left, right))
    }));

    let mut sum = 0;
    let mut curr_crossing = "AAA";
    for turn in first_line.as_bytes().into_iter().cycle() {
        if curr_crossing == "ZZZ" {
            break;
        }
        let crossing = map.get(curr_crossing).unwrap();
        curr_crossing = if turn == &b'L' {
            &crossing.0
        } else {
            &crossing.1
        };
        sum += 1;
    }

    sum
}

#[cfg(test)]
mod tests {
    #[test]
    fn solution() {
        assert_eq!(super::solution("input"), 12169);
    }

    #[test]
    fn solution_test_cases() {
        assert_eq!(super::solution("test0"), 2);
        assert_eq!(super::solution("test1"), 6);
    }
}
