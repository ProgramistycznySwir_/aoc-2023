use std::collections::HashMap;

use text_io::scan;

use crate::read_lines;

#[allow(dead_code)]
pub fn solution(filename: &str) -> i64 {
    let mut file = read_lines(format!("./src/day_8/{}", filename))
        .unwrap()
        .flatten();

    let first_line = file.next().unwrap();

    let map: HashMap<String, (String, String)> = HashMap::from_iter(file.skip(1).map(|line| {
        let crossing_id: String;
        let left: String;
        let right: String;
        scan!(line.bytes() => "{} = ({}, {})", crossing_id, left, right);
        (crossing_id, (left, right))
    }));

    map.keys()
        .filter(|key| key.bytes().skip(2).next().unwrap() == b'A')
        .map(|crossing_id| {
            let mut curr_crossing = crossing_id;
            let mut sum = 0;
            for turn in first_line.as_bytes().into_iter().cycle() {
                if curr_crossing.bytes().skip(2).next().unwrap() == b'Z' {
                    break;
                }
                let crossing = map.get(curr_crossing).unwrap();
                curr_crossing = if turn == &b'L' {
                    &crossing.0
                } else {
                    &crossing.1
                };
                sum += 1;
            }

            sum as i64
        })
        .fold(1i64, least_common_multiplier)
}

fn greatest_common_denominator(mut a: i64, mut b: i64) -> i64 {
    while b != 0 {
        let pom = b;
        b = a % b;
        a = pom;
    }

    return a;
}

fn least_common_multiplier(a: i64, b: i64) -> i64 {
    return a * b / greatest_common_denominator(a, b);
}

#[cfg(test)]
mod tests {
    #[test]
    fn solution() {
        assert_eq!(super::solution("input"), 12030780859469);
    }

    #[test]
    fn solution_test_cases() {
        assert_eq!(super::solution("test2"), 6);
    }

    #[test]
    fn lcm_test() {
        assert_eq!(super::least_common_multiplier(1, 2), 2);
        assert_eq!(super::least_common_multiplier(1, 3), 3);
        assert_eq!(super::least_common_multiplier(2, 3), 6);
        assert_eq!(super::least_common_multiplier(2, 5), 10);
        assert_eq!(super::least_common_multiplier(3, 5), 15);
        assert_eq!(super::least_common_multiplier(3, 7), 21);
        assert_eq!(super::least_common_multiplier(5, 7), 35);
    }
}
