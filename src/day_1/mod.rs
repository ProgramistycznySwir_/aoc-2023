mod part2;
pub use part2::solution_2;

use crate::read_lines;

pub fn is_decimal(c: &char) -> bool {
    c.is_digit(10)
}

#[allow(dead_code)]
pub fn solution_1() -> u32 {
    read_lines("./src/day_1/input")
        .unwrap()
        .flatten()
        .map(|line| {
            let first = line.chars().find(is_decimal).unwrap();
            let second = line.chars().rev().find(is_decimal).unwrap();
            (first, second)
        })
        .map(|(a, b)| a.to_digit(10).unwrap() * 10 + b.to_digit(10).unwrap())
        .sum::<u32>()
}

#[cfg(test)]
mod tests {
    #[test]
    fn solution_1() {
        assert_eq!(super::solution_1(), 55538);
    }

    #[test]
    fn solution_2() {
        assert_eq!(super::part2::solution_2(), 54875);
    }
}
