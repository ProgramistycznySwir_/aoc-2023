use std::collections::HashMap;

use crate::read_lines;

#[allow(dead_code)]
pub fn solution_2() -> u32 {
    let spruce = HashMap::from([
        ('1', TreeNode::Value(1)),
        ('2', TreeNode::Value(2)),
        ('3', TreeNode::Value(3)),
        ('4', TreeNode::Value(4)),
        ('5', TreeNode::Value(5)),
        ('6', TreeNode::Value(6)),
        ('7', TreeNode::Value(7)),
        ('8', TreeNode::Value(8)),
        ('9', TreeNode::Value(9)),
        ('o', TreeNode::Node(HashMap::from([
            ('n', TreeNode::Node(HashMap::from([
                ('e', TreeNode::Value(1)),
            ]))),
        ]))),
        ('t', TreeNode::Node(HashMap::from([
            ('w', TreeNode::Node(HashMap::from([
                ('o', TreeNode::Value(2)),
            ]))),
            ('h', TreeNode::Node(HashMap::from([
                ('r', TreeNode::Node(HashMap::from([
                    ('e', TreeNode::Node(HashMap::from([
                        ('e', TreeNode::Value(3)),
                    ]))),
                ]))),
            ]))),
        ]))),
        ('f', TreeNode::Node(HashMap::from([
            ('o', TreeNode::Node(HashMap::from([
                ('u', TreeNode::Node(HashMap::from([
                    ('r', TreeNode::Value(4)),
                ]))),
            ]))),
            ('i', TreeNode::Node(HashMap::from([
                ('v', TreeNode::Node(HashMap::from([
                    ('e', TreeNode::Value(5)),
                ]))),
            ]))),
        ]))),
        ('s', TreeNode::Node(HashMap::from([
            ('i', TreeNode::Node(HashMap::from([
                ('x', TreeNode::Value(6)),
            ]))),
            ('e', TreeNode::Node(HashMap::from([
                ('v', TreeNode::Node(HashMap::from([
                    ('e', TreeNode::Node(HashMap::from([
                        ('n', TreeNode::Value(7)),
                    ]))),
                ]))),
            ]))),
        ]))),
        ('e', TreeNode::Node(HashMap::from([
            ('i', TreeNode::Node(HashMap::from([
                ('g', TreeNode::Node(HashMap::from([
                    ('h', TreeNode::Node(HashMap::from([
                        ('t', TreeNode::Value(8)),
                    ]))),
                ]))),
            ]))),
        ]))),
        ('n', TreeNode::Node(HashMap::from([
            ('i', TreeNode::Node(HashMap::from([
                ('n', TreeNode::Node(HashMap::from([
                    ('e', TreeNode::Value(9)),
                ]))),
            ]))),
        ]))),
    ]);

    read_lines("./src/day_1/input")
        .unwrap()
        .map(|line| {
            let line = line.unwrap();
            let first = find_seq(&line.chars().collect(), &spruce);
            let second = find_seq_rev(&line.chars().collect(), &spruce);
            (first.to_owned(), second.to_owned())
        })
        .map(|(a, b)| a * 10 + b)
        .sum::<u32>()
}

fn find_seq<'a>(line: &Vec<char>, tree: &'a HashMap<char, TreeNode>) -> &'a u32 {
    for i in 0..line.len() {
        let mut i = i;
        let mut curr_tree = tree;
        while let Some(node) = curr_tree.get(line.get(i).unwrap()) {
            match node {
                TreeNode::Node(map) => {
                    curr_tree = map;
                }
                TreeNode::Value(val) => {
                    return val;
                }
            }
            i += 1;
        }
    }
    panic!();
}

fn find_seq_rev<'a>(line: &Vec<char>, tree: &'a HashMap<char, TreeNode>) -> &'a u32 {
    for i in (0..line.len()).rev() {
        let mut i = i;
        let mut curr_tree = tree;
        while let Some(node) = curr_tree.get(line.get(i).unwrap_or(&'_')) {
            match node {
                TreeNode::Node(map) => {
                    curr_tree = map;
                }
                TreeNode::Value(val) => {
                    return val;
                }
            }
            i += 1;
        }
    }
    panic!();
}

// fn into_seq_finder<'a, T>(tree: HashMap::<char, TreeNode::<&'a T>>) -> impl Fn(impl Iterator<Item = &'a T>) -> T {
//     |iter| find_seq(iter, tree)
// }

// fn find_seq<'a>(mut iter: impl Iterator<Item = char>, tree: Option<&TreeNode>) -> Option<&u32> {
//     // let mut iter = iter.peekable();
//     // println!("{}", iter.peek().unwrap_or(&'_'));
//     // if let Some(node) = tree {
//     //     match node {
//     //         TreeNode::Node(map) => { let next = iter.next(); find_seq(iter, map.get(&next.unwrap())) },
//     //         TreeNode::Value(val) => Some(val)
//     //     }
//     // } else {
//     //     find_seq(iter, tree)
//     // }
// }

// fn find_seq_work<'a>(iter: impl Iterator<Item = char>, tree: Option<&TreeNode>) -> Option<&u32> {
//     // let mut iter = iter.peekable();
//     // println!("{}", iter.peek().unwrap_or(&'_'));
//     if let Some(node) = tree {
//         match node {
//             TreeNode::Node(map) => { let next = iter.next(); find_seq(iter, map.get(&next.unwrap())) },
//             TreeNode::Value(val) => Some(val)
//         }
//     } else {
//         find_seq(iter, tree)
//     }
// }

enum TreeNode {
    Node(HashMap<char, TreeNode>),
    Value(u32),
}

// fn find_seq<'a, TKey, TRes>(iter: impl Iterator<Item = &'a TKey>, tree: HashMap::<TKey, TreeNode::<&'a TKey, TRes>>) -> TRes {
//     let mut curr = tree;
//     for c in iter {
//         if let res = curr.get(c) {

//         }
//     }

//     123
// }

// enum TreeNode<TKey, TRes> {
//     Node(HashMap::<TKey, TreeNode::<TKey, TRes>>),
//     Value(TRes),
// }
