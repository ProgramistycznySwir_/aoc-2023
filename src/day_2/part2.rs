

use std::{collections::HashMap, cmp::max};
use super::read_lines;


// Gave up atm.
#[allow(dead_code)]
pub fn solution_2() -> i32 {
    read_lines("./src/day_2/input")
        .unwrap()
        .flatten()
        .map(get_power_of_games)
        .sum()
}

fn get_power_of_games(line: String) -> i32 {
    let mut max_cubes = HashMap::from([("red", 0), ("green", 0), ("blue", 0)]);

    let normalized = line.replace(&[':', ',', ';'], "");
    let words = normalized.split(' ').collect::<Vec<&str>>();
    let pairs = words.chunks(2);

    for pair in pairs.skip(1) {
        let amount = pair[0].parse::<i32>().unwrap();
        let color = pair[1];

        max_cubes.insert(color, max(max_cubes[color], amount));
    }

    max_cubes.values().fold(1, std::ops::Mul::mul)
}
