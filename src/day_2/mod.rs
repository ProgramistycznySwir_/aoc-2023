use std::collections::HashMap;

use crate::read_lines;

mod part2;
pub use part2::solution_2;

#[allow(dead_code)]
pub fn solution_1() -> i32 {
    read_lines("./src/day_2/input")
        .unwrap()
        .flatten()
        .map(flatten_valid_game_id)
        .flatten()
        .sum()
}

fn flatten_valid_game_id(line: String) -> Option<i32> {
    let total_cubes = HashMap::from([("red", 12), ("green", 13), ("blue", 14)]);

    let normalized = line.replace(&[':', ',', ';'], "");
    let words = normalized.split(' ').collect::<Vec<&str>>();
    let pairs = words.chunks(2);

    for pair in pairs.skip(1) {
        let amount = pair[0];
        let color = pair[1];

        if amount.parse::<i32>().unwrap() > *total_cubes.get(color).unwrap() {
            return None;
        }
    }

    Some(words[1].parse::<i32>().unwrap())
}


#[cfg(test)]
mod tests {
    #[test]
    fn solution_1() {
        assert_eq!(super::solution_1(), 2505);
    }

    #[test]
    fn solution_2() {
        assert_eq!(super::part2::solution_2(), 70265);
    }
}
